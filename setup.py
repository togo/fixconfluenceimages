from distutils.core import setup
import py2exe

setup(windows=[{"script": "FixConfluenceImages.py"}], options={"py2exe": {"includes": ["sip", "PyQt4.QtCore"]}})
