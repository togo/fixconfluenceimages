import email
import io
import os
import quopri
import uuid
import configparser

from PyQt4.QtGui import *
from bs4 import BeautifulSoup

from confluence import Confluence


class Attachment:
    def __init__(self, name, page_id, version, type):
        self.name = name
        self.page_id = page_id
        self.version = version
        self.type = type

    def __hash__(self):
        return hash((self.name, self.page_id, self.version, self.type))

    def __eq__(self, other):
        return (self.name, self.page_id, self.version, self.type) == (
            other.name, other.page_id, other.version, other.type)

    def __ne__(self, other):
        # Not strictly necessary, but to avoid having both x==y and x!=y
        # True at the same time
        return not (self == other)


class LoginWindow(QDialog):

    def __init__(self, parent=None, url="", login="", password=""):
        super(LoginWindow, self).__init__(parent)

        self.login = login
        self.password = password
        self.url = url

        flo = QFormLayout()

        e1 = QLineEdit()
        e1.setText(self.url)
        e1.textChanged.connect(self.__set_url)
        flo.addRow("Confluence URL", e1)

        e2 = QLineEdit()
        e2.setText(self.login)
        e2.textChanged.connect(self.__set_login)
        flo.addRow("Username", e2)

        e3 = QLineEdit()
        e3.setText(self.password)
        e3.textChanged.connect(self.__set_password)
        e3.setEchoMode(QLineEdit.Password)
        flo.addRow("Password", e3)

        self.setLayout(flo)
        self.setWindowTitle("Enter Confluence Credentials")

        layout = QGridLayout()
        flo.addRow(layout)
        self.but_ok = QPushButton("OK")
        layout.addWidget(self.but_ok, 0, 1)
        self.but_ok.clicked.connect(self.accept)

        self.but_cancel = QPushButton("Cancel")
        layout.addWidget(self.but_cancel, 0, 2)
        self.but_cancel.clicked.connect(self.reject)

    def __set_url(self, text):
        self.url = text

    def __set_login(self, text):
        self.login = text

    def __set_password(self, text):
        self.password = text


if __name__ == '__main__':

    import sys

    app = QApplication(sys.argv)

    # Read credentials
    config_path = r'config.ini'
    config = configparser.ConfigParser()
    if os.path.isfile(config_path):
        config.read(config_path)
    else:
        config['confluence'] = {'url': 'https://xxx.xxx.com/confluence', 'user': '<username>', 'pass': '<password'}

    login = LoginWindow(url=config['confluence']['url'], login=config['confluence']['user'],
                        password=config['confluence']['pass'])
    if login.exec_() != QDialog.Accepted:
        sys.exit()

    config['confluence']['url'] = login.url
    config['confluence']['user'] = login.login
    config['confluence']['pass'] = login.password
    with open(config_path, 'w') as configfile:
        config.write(configfile)

    confluence = Confluence(profile="confluence")

    filePath = str(QFileDialog.getOpenFileName(None, 'Select Exported Word File', '', 'Word Documents (*.doc)'))
    if not os.path.isfile(filePath):
        sys.exit()

    # Open MIME message document
    with open(filePath, 'rb') as f:
        msg = email.message_from_binary_file(f)

    # Index all content locations
    payloads = {}
    for p in msg.get_payload():
        payloads[p['Content-Location']] = p

    # Parse document HTML
    main_part = msg.get_payload()[0]
    html_doc = main_part.get_payload(decode=True)
    doc = BeautifulSoup(html_doc, 'lxml')

    # Locate all missing images and store in missing_images
    images = doc.find_all("img", class_="confluence-embedded-image")
    missing_images = {}
    for image in images:
        src = image.attrs['src']
        local_src = 'file:///C:/' + src
        if local_src not in payloads:
            name = image.attrs['data-linked-resource-default-alias']
            page_id = image.attrs['data-linked-resource-container-id']
            version = image.attrs['data-linked-resource-version']
            mime_type = image.attrs['data-linked-resource-content-type']
            att = Attachment(name, page_id, version, mime_type)
            if att not in missing_images:
                missing_images[att] = [image]
            else:
                missing_images[att].append(image)

    # Fetch missing images
    for (attachment, images) in missing_images.items():
        data = confluence.getAttachedFileById(attachment.page_id, attachment.name, attachment.version)

        # Create attachment ID
        location = uuid.uuid4().hex

        # Append to MIME message
        part = email.message.Message()
        part.add_header('Content-Type', attachment.type)
        part.add_header('Content-Location', 'file:///C:/' + location)
        part.set_payload(payload=data.data)
        email.encoders.encode_base64(part)
        msg.attach(part)

        # Repoint <img src="..."/> to embedded image
        for image in images:
            image.attrs['src'] = location
            image.attrs['height'] = '100%'
            image.attrs['width'] = '100%'

    # Update main part
    main_part.set_payload(quopri.encodestring(bytes(str(doc), encoding='utf-8'), quotetabs=False))

    # Write to file
    dir, filename = os.path.split(filePath)
    stem, ext = os.path.splitext(filename)
    outPath = os.path.join(dir, '{}-fixed{}'.format(stem, ext))

    with io.open(outPath, 'w', newline='\r\n') as f:
        f.write(msg.as_string())
